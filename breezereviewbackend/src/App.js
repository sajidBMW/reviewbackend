import React from "react";
import "./App.css";
import Home from "./modules/home/components/home";

// optional configuration

function App() {
  return (
    <div>
      <Home />
    </div>
  );
}

export default App;
