import React from "react";
import "./view-location-users.css";
import MaterialTable from "material-table";
import { DeleteLocationUsers, UpdateLocation, GetLocationUsers } from "../homeService";
import { ModalDisplay,Header,createNotification } from "../reusable/reusable";
import { NotificationContainer } from "react-notifications";

export default class ViewLocationUsers extends React.Component {
  constructor(props) {
    super(props);
    this.state = { company: [], editeddata: ""};
    this.modalshowid = '02';
  }

  async componentDidMount() {
    // GetCompanyApi(res => this.setState({list: res}))
    let data = await GetLocationUsers();
    let companydata = data.data.data;
   // console.log(companydata)
    this.setState({ companydata });
  }


  handleClickDelete = async (event) => {
    // console.log(event.id);
    let deleteddata = await DeleteLocationUsers(event.id);
    let data = await GetLocationUsers();
    let companydata = data.data.data;
    //console.log(companydata)
    this.setState({ companydata });
    await createNotification("warning", deleteddata.message);
  };



  render() {
    return (
      <>
       <NotificationContainer />
      <div
        className="w3-main"
        style={{ marginLeft: "330px", marginTop: "43px", marginRight: "30px" }}
      >
       <Header name="View Company Account"/>
        
          <MaterialTable
            title=""
            columns={[
              { title: "ID", field: "id" },
              { title: "User Name", field: "username" },
              { title: "Location", field: "location_title" },
            ]}
            data={this.state.companydata}
            actions={[
              (rowData) => ({
                icon: "delete",
                tooltip: "Delete User",
                onClick: (event, rowData) => this.handleClickDelete(rowData),
                disabled: rowData.birthYear < 2000,
              }),
            ]}
            options={{
              actionsColumnIndex: -1,
            }}
          />
        </div>
      
      </>
    );
  }
}
