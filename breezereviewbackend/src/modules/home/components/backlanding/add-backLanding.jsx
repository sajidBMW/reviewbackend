import React  from "react";
import "./add-backLanding.css";
import {NotificationContainer} from 'react-notifications';
import { Header,createNotification } from "../reusable/reusable";
import { CreateBackLandingPage,GetLocationApi } from "../homeService";
import Select from 'react-select'

export default class AddBackLandingPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {location_id:'', options:[], message:'',bgcolor:'', formErrors: { message: "",bgcolor:"",file:""},file:null};
  }
  

  async componentDidMount() {
    // GetCompanyApi(res => this.setState({list: res}))
    let data = await GetLocationApi();
    let companydata = await data.data.data;

    

    var output = await companydata.map(s => ({label:s.title,value:s.id}));
    //console.log(output);
    this.setState({ options : output});

  }


  formValid = ({ formErrors }) => {
    // console.log(formErrors);
    let valid = true;

    Object.values(formErrors).forEach((val) => {
      // console.log(val)
      val.length > 0 && (valid = false);
    });
    return valid;
  };

  handleChange = (event) => {
    event.preventDefault();
    const { name, value } = event.target;
    this.setState({ [name]: value }, () => {
      //console.log(this.state);
    });
    //console.log(this.state)
    let newform = this.state.formErrors;
    switch (name) {
      case "message":
        newform.message =  value.length > 0 ? "" : "Message field cannot be empty";
        break;

        case "bgcolor":
        newform.bgcolor = value.length > 0 ? "" : "Bgcolor field cannot be empty";
        break;

      default:
        break;
    }
    this.setState({ newform, [name]: value });
  };



   handleSubmit = async (event) => {
    // console.log(this.state)
    event.preventDefault();
   
    if (this.formValid(this.state) && this.state.location_id !== "") {
      const formdata = new FormData() 
      if(this.state.file !== null)
      {
        formdata.append('image', this.state.file);
      }
      formdata.append('message', this.state.message);
      formdata.append('bgcolor', this.state.bgcolor);
      formdata.append('location_id', this.state.location_id);

      let data = await CreateBackLandingPage(formdata)
      // console.log(data)
      if(data.success === true){
        //debugge
        this.setState({message:'',bgcolor:'',location_id:''})
        
      document.getElementById("add-Location").reset();
      this.setState({startDate:''})
        await createNotification('success',data.message);
      }
      else
      {
        await createNotification('error',data.message);
      }
    } else {
    
      // console.log("Form is invalid");
    }
  };




  handleFile = async (event) =>{

      let file = event.target.files[0];
      // console.log(file);
      this.setState({file:file})
    
      
  }

  selectChangeCompnay = selectedOption => {
    // console.log(selectedOption)
    this.setState(
      { selectedOption },
      () => 
      this.setState({location_id:this.state.selectedOption.value})
      //console.log(this.state.company_id)
    );
  };


  render() {
    const { selectedOption } = this.state;

    return (
     
      <>
        <div
          className="w3-main"
          style={{ marginLeft: "300px", marginTop: "43px" }}
        >
          <Header name="Add Back Landing Page Setting"/>

          <div className="w3-container  w3-margin-top">
            <form className="w3-container w3-card-4" id="add-Location">
            <p> <label>Select Location</label></p>
           

            <Select
                options={this.state.options}
                onChange={this.selectChangeCompnay}
              />

              <p>
                <label>Message</label>
                <input
                  className="w3-input"
                  name="message"
                  type="text"
                  onChange={this.handleChange}
                  noValidate
                />
                {this.state.formErrors.message.length > 0 && (
                  <span className="" style={{ color: "red" }}>
                    {this.state.formErrors.message}
                    <br />
                  </span>
                )}
              </p>


              <p>
                <label>bgcolor</label>
                <input
                  className="w3-input"
                  name="bgcolor"
                  type="text"
                  onChange={this.handleChange}
                  noValidate
                />
                {this.state.formErrors.bgcolor.length > 0 && (
                  <span className="" style={{ color: "red" }}>
                    {this.state.formErrors.bgcolor}
                    <br />
                  </span>
                )}
              </p>

  
              <p>
                <label>File</label>
                <input
                  className="w3-input"
                  type="file"
                  name="file"
                  onChange={this.handleFile}
                  noValidate
                />
                {this.state.formErrors.file.length > 0 && (
                  <span className="" style={{ color: "red" }}>
                    {this.state.formErrors.file}
                    <br />
                  </span>
                )}
              </p>



              <p>
                <button
                  className="w3-button w3-section w3-black w3-ripple"
                  disabled={this.state.message === '' || this.state.bgcolor === '' || this.state.location_id === '' }
                  onClick={this.handleSubmit}
                >
                  Add
                </button>
              </p>
            </form>
          </div>
          <NotificationContainer/>
        </div>
      
      </>
     
    );
  }
}
