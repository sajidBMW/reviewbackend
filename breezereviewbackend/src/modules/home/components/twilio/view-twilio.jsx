import React from "react";
import "./view-twilio.css";
import MaterialTable from "material-table";
import { DeleteTwilio, GetTwilioApi } from "../homeService";
import { Header,createNotification } from "../reusable/reusable";
import { NotificationContainer } from "react-notifications";

export default class ViewTwilio extends React.Component {
  constructor(props) {
    super(props);
    this.state = { company: []};
  }

  async componentDidMount() {
    // GetCompanyApi(res => this.setState({list: res}))
    let data = await GetTwilioApi();
    let companydata = data.data.data;
   // console.log(companydata)
    this.setState({ companydata });
  }



  handleClickDelete = async (event) => {
   // console.log(event.id);
    let deleteddata = await DeleteTwilio(event.id);
    let data = await GetTwilioApi();
    let companydata = data.data.data;
    //console.log(companydata)
    this.setState({ companydata });
    createNotification("warning", deleteddata.message);
  };


  render() {
    return (
      <>
       <NotificationContainer />
      <div
        className="w3-main"
        style={{ marginLeft: "330px", marginTop: "43px", marginRight: "30px" }}
      >
       <Header name="View Twilio Details"/>

          <MaterialTable
            title=""
            columns={[
              { title: "ID", field: "id" },
              { title: "Location", field: "location_title" },
              { title: "Number", field: "number" },
              { title: "Secure ID", field: "sid",render: rowData =>  `${rowData.sid.substring(0,10)}...`},
              { title: "Secure Token", field: "token" , render: rowData =>  `${rowData.token.substring(0,10)}...`},
            ]}
            data={this.state.companydata}
            actions={[
          

              (rowData) => ({
                icon: "delete",
                tooltip: "Delete User",
                onClick: (event, rowData) => this.handleClickDelete(rowData),
                disabled: rowData.birthYear < 2000,
              }),
            ]}
            options={{
              actionsColumnIndex: -1,
            }}
          />
        </div>
      
      </>
    );
  }
}
