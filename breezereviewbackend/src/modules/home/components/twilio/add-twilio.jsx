import React  from "react";
import "./add-twilio.css";
import {NotificationContainer} from 'react-notifications';
import { Header,createNotification } from "../reusable/reusable";
import { GetLocationApi,CreateTwilio } from "../homeService";

import Select from 'react-select'

export default class AddTwilio extends React.Component {
  constructor(props) {
    super(props);
    this.state = { options: [],company_id:'',sid:'',token: "",number:"", formErrors: { sid: "",token:"",number:"",selectedOption:"" },selectedOption: null};
  }

  

  async componentDidMount() {
    // GetCompanyApi(res => this.setState({list: res}))
    let data = await GetLocationApi();
    let companydata = await data.data.data;

    var output = await companydata.map(s => ({label:s.title,value:s.id}));
    //console.log(output);
    this.setState({ options : output});
    //this.setState({ companydata });
  }


  formValid = ({ formErrors }) => {
    // console.log(formErrors);
    let valid = true;

    Object.values(formErrors).forEach((val) => {
      // console.log(val)
      val.length > 0 && (valid = false);
    });
    return valid;
  };

  handleChange = (event) => {
    event.preventDefault();
    const { name, value } = event.target;
    this.setState({ [name]: value }, () => {
      //console.log(this.state);
    });
    //console.log(this.state)
    let newform = this.state.formErrors;
    switch (name) {
      case "sid":
        newform.sid = value.length > 0 ? "" : "SID field cannot be empty";
        break;

        case "token":
        newform.token = value.length > 0 ? "" : "Token field cannot be empty";
        break;
        
        case "number":
          newform.number = value.length > 0 ? "" : "Number not selected";
          break;

      default:
        break;
    }
    this.setState({ newform, [name]: value });
  };



   handleSubmit = async (event) => {
    
    event.preventDefault();
   
    if (this.formValid(this.state) && this.state.company_id != "") {
      let data = await CreateTwilio(this.state.company_id,this.state.sid,this.state.token,this.state.number)
      // console.log(data)
      if(data.success == true){
        //debugger
       // document.getElementById("add-company").reset();
      //  this.setState({company_name:''})
        
      document.getElementById("add-Location").reset();
      this.setState({startDate:''})
        await createNotification('success',data.message);
      }
      else
      {
        await createNotification('error',data.message);
      }
    } else {
    
      // console.log("Form is invalid");
    }
  };

  selectChange = selectedOption => {
    // console.log(selectedOption)
    this.setState(
      { selectedOption },
      () => 
      this.setState({company_id:this.state.selectedOption.value})
      //console.log(this.state.company_id)
    );
  };



  render() {
    const { selectedOption } = this.state;
    return (
     
      <>
        <div
          className="w3-main"
          style={{ marginLeft: "300px", marginTop: "43px" }}
        >
          <Header name="Add Twilio Information"/>

          <div className="w3-container  w3-margin-top">
            <form className="w3-container w3-card-4" id="add-Location">

            <p>
                <label>Select Location</label>
              </p>
              <Select options={this.state.options} onChange={this.selectChange} value={selectedOption}/>


              <p>
                <label>SID</label>
                <input
                  className="w3-input"
                  name="sid"
                  type="text"
                  onChange={this.handleChange}
                  noValidate
                />
                {this.state.formErrors.sid.length > 0 && (
                  <span className="" style={{ color: "red" }}>
                    {this.state.formErrors.sid}
                    <br />
                  </span>
                )}
              </p>


              <p>
                <label>Token</label>
                <input
                  className="w3-input"
                  name="token"
                  type="text"
                  onChange={this.handleChange}
                  noValidate
                />
                {this.state.formErrors.token.length > 0 && (
                  <span className="" style={{ color: "red" }}>
                    {this.state.formErrors.token}
                    <br />
                  </span>
                )}
              </p>

              <p>
                <label>Number</label>
                <input
                  className="w3-input"
                  name="number"
                  type="text"
                  onChange={this.handleChange}
                  noValidate
                />
                {this.state.formErrors.number.length > 0 && (
                  <span className="" style={{ color: "red" }}>
                    {this.state.formErrors.number}
                    <br />
                  </span>
                )}
              </p>


              <p>
                <button
                  className="w3-button w3-section w3-black w3-ripple"
                  disabled={this.state.sid == '' || this.state.token == '' || this.state.number == '' || this.state.company_id == ''}
                  onClick={this.handleSubmit}
                >
                  Add Twilio
                </button>
              </p>
            </form>
          </div>
          <NotificationContainer/>
        </div>
      
      </>
     
    );
  }
}
