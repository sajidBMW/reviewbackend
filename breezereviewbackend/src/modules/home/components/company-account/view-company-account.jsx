import React from "react";
import "./view-company-account.css";
import MaterialTable from "material-table";
import { DeleteCompanyAccount, UpdateLocation, GetAccountsApiList } from "../homeService";
import { ModalDisplay,Header,createNotification } from "../reusable/reusable";
import { NotificationContainer } from "react-notifications";

export default class ViewCompanyAccount extends React.Component {
  constructor(props) {
    super(props);
    this.state = { company: [], editeddata: ""};
    this.modalshowid = '02';
  }

  async componentDidMount() {
    // GetCompanyApi(res => this.setState({list: res}))
    let data = await GetAccountsApiList();
    let companydata = data.data.data;
   // console.log(companydata)
    this.setState({ companydata });
  }


  handleClickDelete = async (event) => {
    // console.log(event.id);
    let deleteddata = await DeleteCompanyAccount(event.id);
    let data = await GetAccountsApiList();
    let companydata = data.data.data;
    //console.log(companydata)
    this.setState({ companydata });
    await createNotification("warning", deleteddata.message);
  };



  render() {
    return (
      <>
       <NotificationContainer />
      <div
        className="w3-main"
        style={{ marginLeft: "330px", marginTop: "43px", marginRight: "30px" }}
      >
       <Header name="View Company Account"/>
        
          <MaterialTable
            title=""
            columns={[
              { title: "ID", field: "id" },
              { title: "Location", field: "location_title" },
              { title: "Type", field: "type" },
            ]}
            data={this.state.companydata}
            actions={[
              (rowData) => ({
                icon: "delete",
                tooltip: "Delete User",
                onClick: (event, rowData) => this.handleClickDelete(rowData),
                disabled: rowData.birthYear < 2000,
              }),
            ]}
            options={{
              actionsColumnIndex: -1,
            }}
          />
        </div>
      
      </>
    );
  }
}
