import React  from "react";
import {NotificationContainer} from 'react-notifications';
import { Header,createNotification } from "../reusable/reusable";
import { GetLocationApi,CreateCustom } from "../homeService";

import Select from 'react-select'

export default class AddCustom extends React.Component {
    
  state = { 
    options: [],
    locationId:'',
    businessLink:'', 
    formErrors: { businessLink: "",selectedOption:"" },
    selectedOption: null
  }

  async componentDidMount() {
    let data = await GetLocationApi();
    let companydata = await data.data.data;
    var output = await companydata.map(s => ({label:s.title,value:s.id}));
    this.setState({ options : output});
  }

  formValid = ({ formErrors }) => {
    let valid = true;
    Object.values(formErrors).forEach((val) => {
      val.length > 0 && (valid = false);
    });
    return valid;
  };

  handleChange = (event) => {
    event.preventDefault();
    const { name, value } = event.target;
    this.setState({ [name]: value }, () => {
    });
    let newform = this.state.formErrors;
    switch (name) {
      case "businessLink":
        newform.businessLink = value.length > 0 ? "" : "Business Link field cannot be empty";
        break;
      default:
        break;
    }
    this.setState({ newform, [name]: value });
  };

   handleSubmit = async (event) => {
    event.preventDefault();
   
    if (this.formValid(this.state) && this.state.locationId != "") {
      let data = await CreateCustom(this.state.locationId,this.state.businessLink)
      if(data.success == true){
      document.getElementById("add-Custom").reset();
      this.setState({startDate:''})
        await createNotification('success',data.message);
      }
      else
      {
        await createNotification('error',data.message);
      }
    }
  };

  selectChange = selectedOption => {
    this.setState(
      { selectedOption },
      () => 
      this.setState({locationId:this.state.selectedOption.value})
    );
  };



  render() {
    const { selectedOption } = this.state;
    return ( 
      <>
        <div
          className="w3-main"
          style={{ marginLeft: "300px", marginTop: "43px" }}
        >
          <Header name="Add Custom Link"/>
          <div className="w3-container  w3-margin-top">
            <form className="w3-container w3-card-4" id="add-Custom">
            <p>
                <label>Select Location</label>
              </p>
              <Select options={this.state.options} onChange={this.selectChange} value={selectedOption}/>
              <p>
                <label>Business Link</label>
                <input
                  className="w3-input"
                  name="businessLink"
                  type="text"
                  onChange={this.handleChange}
                  noValidate
                />
                {this.state.formErrors.businessLink.length > 0 && (
                  <span className="" style={{ color: "red" }}>
                    {this.state.formErrors.businessLink}
                    <br />
                  </span>
                )}
              </p>
              <p>
                <button
                  className="w3-button w3-section w3-black w3-ripple"
                  disabled={this.state.businessLink == '' || this.state.locationId == ''}
                  onClick={this.handleSubmit}
                >
                  Add Custom Link
                </button>
              </p>
            </form>
          </div>
          <NotificationContainer/>
        </div>     
      </>   
    );
  }
}
